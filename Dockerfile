FROM nginx:1.15.2-alpine
COPY ./public /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]