import personList from "./PersonList/reducer";
import auth from "./Auth/reducer";
import theme from "./Theme/reducer"
import language from "./Language/reducer"

export default { auth, personList, theme, language};