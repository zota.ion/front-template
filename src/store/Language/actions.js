import actionTypes from "./actionTypes"

export const setLanguage = (lang) => {
    localStorage.setItem("lang",lang);    
    return {
        type: actionTypes.SET_LANGUAGE,
        lang: lang
    }
}

export const languageCheckState = () => {
    return dispatch => {
        const lang = localStorage.getItem("lang");        
        if (lang !== false && lang !== null) {
           dispatch(setLanguage(lang))
        }
    };
};