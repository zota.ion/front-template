import actionTypes from "./actionTypes"

export const setTheme = (theme) => {
    localStorage.setItem("myTheme",theme);
    return {
        type: actionTypes.SET_THEME,
        theme: theme
    }
}

export const themeCheckState = () => {
    return dispatch => {        
        const theme = localStorage.getItem("myTheme");
        
        if (theme !== false && theme !== null) {
           dispatch(setTheme(theme))
        }
    };
};