import actionTypes from "./actionTypes";
import { updateObject } from "../utility";

const initialState = {
    myTheme: "darkTheme"
};

const setMyTheme = ( state, action ) => {
    return updateObject( state, { myTheme: action.theme } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_THEME: return setMyTheme(state, action);
        
        default:
            return state;
    }
};

export default reducer;