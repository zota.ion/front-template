const actions = {AUTH_START :"AUTH_START",
                 AUTH_SUCCESS : "AUTH_SUCCESS",
                 AUTH_FAIL : "AUTH_FAIL",
                 AUTH_LOGOUT: "AUTH_LOGOUT",
                 SET_AUTH_REDIRECT_PATH: "SET_AUTH_REDIRECT_PATH",
                }

export default actions;                 