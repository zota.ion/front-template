const path = require("path");
const manageTranslations = require("react-intl-translations-manager").default;

manageTranslations({
  messagesDirectory: path.join(__dirname, "messages"),
  translationsDirectory: path.join(__dirname, "locales"),
  languages: ["en", "ro"] // any language you need
});