import en from "./en.json";
import ro from "./ro.json";
import { addLocaleData } from "react-intl";
import roLocaleData from "react-intl/locale-data/ro";
import enLocaleData from "react-intl/locale-data/en";

addLocaleData(roLocaleData);
addLocaleData(enLocaleData);

export default {en, ro};