import { createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import React, { Component } from "react";
import { ApolloProvider } from "react-apollo";
import { IntlProvider } from "react-intl";
import PublicRoutes from "./router";
import themes from "./themes";
import translations from "./translation/locales";
import {connect} from "react-redux";
import * as actions from "./store/actions" 
import client from "./graphql/ApolloClient";

class App extends Component {  

  componentDidMount(){

    this.props.initLang();
    this.props.initTheme();
    this.props.initToken();  
    
  }

  render() {        
    const messages = translations[this.props.lang];    
    const theme = createMuiTheme(themes[this.props.myTheme]);  
    return ( 
    <IntlProvider locale={this.props.lang} key={this.props.lang} messages={messages}>
      <MuiThemeProvider theme={theme}>
        <ApolloProvider client={client({token: this.props.token})}>
          <PublicRoutes isLoggedIn={this.props.isAuthenticated}/>
       </ApolloProvider>
      </MuiThemeProvider>
    </IntlProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
      isAuthenticated: state.auth.token !== null,
      token: state.auth.token,
      lang: state.language.lang,
      myTheme: state.theme.myTheme,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    initTheme: () => dispatch(actions.themeCheckState()),
    initLang: () => dispatch(actions.languageCheckState()),
    initToken: () => dispatch(actions.authCheckState()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
