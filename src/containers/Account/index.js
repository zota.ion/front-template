import React, { Component } from "react"
import { graphql } from "react-apollo";
import { LinearProgress } from "@material-ui/core";
import gql from "graphql-tag";

const getUser = () =>{
  return gql`query {
    user: findUserById(id: 1) {
      id
      firstName
      lastName
    }
  }`
}

class Account extends Component {

  render() {
    if (this.props.data.loading) {
      return <LinearProgress/>;
    } else {

    return (
      <div className="root">       
       Account 
       <h2>userId: {this.props.data.user.id}</h2><br/>
       <h2>firstName: {this.props.data.user.firstName}</h2><br/>
       <h2>lastName: {this.props.data.user.lastName}</h2>
      </div>
    )
    }
  }
}

export default graphql(getUser())(Account)