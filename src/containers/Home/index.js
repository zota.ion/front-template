import React, { Component } from "react";
import MessageProvider from "../../components/util/MessageProvider";

export default class Home extends Component {
  render() {
    return (
      <div>
       Home 
       <br/>
       <MessageProvider id="app.title"/>
      </div>
    )
  }
}
