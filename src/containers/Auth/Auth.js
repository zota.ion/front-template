import React,{Component} from "react";
import PropTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import * as actions from "../../store/Auth/actions";
import {connect} from "react-redux";
import { Field, Form, Formik } from "formik";
import { TextField } from "formik-material-ui";
import { LinearProgress } from "@material-ui/core";
import MySnackbarContentWrapper from "../../hoc/MySnackbarContentWrapper";
import Snackbar from "@material-ui/core/Snackbar";
import { Redirect } from "react-router-dom";
import analitics from "../../helpers/GoogleAnalitics";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

class Auth extends Component {

  state = {
    open: true
  }

  componentDidMount() {
    if (this.props.authRedirectPath !== "/main") {
        this.props.onSetAuthRedirectPath();
    }
}

  submitHandler = (values, {setSubmitting}) => {
    this.props.onAuth(values.email,values.password)
    this.setState({open:true});
    setSubmitting(false);
  }

  validation = (values) => {    
      const errors = {}
      if (!values.email) {
        errors.email = "Required";
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ) {
        errors.email = "Invalid email address";
      }
      return errors;    
  }

  progress = () => {
    if(this.props.loading)
    {
    return(<div><br/><br/><LinearProgress/></div>)
    } else {
      return(<div><br/><br/><br/></div>)
    }
  }

  handleSnackbarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  initialValues = {email: "", password: ""}
 
  render(){
  analitics();
  const classes = this.props.classes;

  let authRedirect = null;
  if (this.props.isAuthenticated) {
    authRedirect = <Redirect to={this.props.authRedirectPath}/>
  }  

  return (
    <main className={classes.main}>
    {authRedirect}
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.props.error === "Unauthorized" && 
          this.state.open ? true : false}
          autoHideDuration={6000}
          onClose={this.handleSnackbarClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleSnackbarClose}
            variant="error"
            message="Wrong Email or Password!"
          />
        </Snackbar>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.props.error !== "Unauthorized" &&
                this.state.open &&
                this.props.error !== null ? true : false}
          autoHideDuration={6000}
          onClose={this.handleSnackbarClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleSnackbarClose}
            variant="warning"
            message="Something went wrong, please try again later."
          />
        </Snackbar>
        <Formik
        initialValues={this.initialValues}
        validate={this.validation}
        onSubmit={this.submitHandler}
        render={({submitForm, isSubmitting}) => ( 
                  <Form>
                    <Field
                      name="email"
                      type="email"
                      label="Email"
                      component={TextField}
                    />
                    <br />
                    <Field
                      type="password"
                      label="Password"
                      name="password"
                      component={TextField}
                    />     
                    <br /><br />
                    <Button
                      variant="contained"
                      color="primary"
                      disabled={isSubmitting}  
                      onClick={submitForm}           
                    >
                      Submit
                    </Button>  
                    {this.progress()}           
                  </Form>
            )          
        }        
       />   
      </Paper>
    </main>
  )}
}

Auth.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email,password) => dispatch(actions.auth(email,password)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/main"))
  }
}

const mapStateToProps = state => {
  return {
      loading: state.auth.loading,
      error: state.auth.error,
      isAuthenticated: state.auth.token !== null,
      authRedirectPath: state.auth.authRedirectPath      
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Auth));