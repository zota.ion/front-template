import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "../../store/Auth/actions";

class Logout extends Component {
    componentDidMount () {
        this.props.onLogout();        
    }

    render () {    
        return <Redirect to="/signin"/>;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(actions.logout())
    };
};

export default connect(null, mapDispatchToProps)(Logout);