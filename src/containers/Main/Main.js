import React,{Component} from "react";
import MainLayout from "../../hoc/MainLayout"
import MainRouter from "./MainRouter";

class Main extends Component{    

    render () {
        const { url } = this.props.match;
        return(
            <MainLayout>            
                <MainRouter url = {url}/>            
            </MainLayout>);
    }
}

export default Main;