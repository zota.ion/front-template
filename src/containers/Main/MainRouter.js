import React, { Component } from "react";
import { Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncLoader";

const routes = [
  {
    path: "",
    component: asyncComponent(() => import("../Home")),
  },
  {
    path: "account",
    component: asyncComponent(() => import("../Account")),
  },
  {
    path: "profile",
    component: asyncComponent(() => import("../Profile")),
  },
  {
    path: "messages",
    component: asyncComponent(() => import("../Messages")),
  },
  {
    path: "notifications",
    component: asyncComponent(() => import("../Notifications")),
  },
  {
    path: "person",
    component: asyncComponent(() => import("../Persons")),
  },
  {
    path: "inbox",
    component: asyncComponent(() => import("../Inbox")),
  },
  
];

class MainRouter extends Component {
  render() {      
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={path}
              path={`${url}/${path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default MainRouter;
