import aaronTheme from "./AaronTheme";
import coralTheme from "./CoralTheme";
import darkTheme from "./DarkTheme";
import greenTheme from "./GreenTheme";
import halloweenTheme from "./HalloweenTheme";
import oceanTheme from "./OceanTheme";
import pastelTheme from "./PastelTheme";
import rainbowTheme from "./RainbowTheme";

export default {greenTheme,aaronTheme,coralTheme,darkTheme,oceanTheme,pastelTheme,halloweenTheme,rainbowTheme}