const oceanTheme = {
    "name":"Ocean theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"default": "rgba(192, 232, 240, 1)", "paper": "#fafafa"},
            "primary": {
                    "light": "rgba(94, 145, 243, 1)",
                    "main": "rgba(76, 112, 166, 1)",
                    "dark": "rgba(57, 82, 118, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(94, 145, 243, 1)",
                    "main": "rgba(76, 112, 166, 1)",
                    "dark": "rgba(57, 82, 118, 1)",
                    "contrastText": "#fff"
            },
            "error": {"light": "#e57373", "main": "#f44336", "dark": "#d32f2f", "contrastText": "#fff"},
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default oceanTheme;