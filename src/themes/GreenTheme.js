
 const greenTheme = {
    "name":"Green theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "#fff", "default": "#fafafa"},
            "primary": {
                    "light": "rgba(101, 163, 128, 1)",
                    "main": "rgba(68, 127, 112, 1)",
                    "dark": "rgba(17, 76, 68, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(101, 163, 128, 1)",
                    "main": "rgba(68, 127, 112, 1)",
                    "dark": "rgba(17, 76, 68, 1)",
                    "contrastText": "#fff"
            },
            "error": {"light": "#e57373", "main": "#f44336", "dark": "#d32f2f", "contrastText": "#fff"},
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default greenTheme;