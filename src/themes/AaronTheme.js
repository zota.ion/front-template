const aaronTheme = {
    "name":"Aaron theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "rgba(250, 250, 250, 1)", "default": "rgba(216, 206, 206, 1)"},
            "primary": {
                    "light": "rgba(85, 116, 111, 1)",
                    "main": "rgba(27, 62, 60, 1)",
                    "dark": "rgba(12, 41, 37, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(85, 116, 111, 1)",
                    "main": "rgba(27, 62, 60, 1)",
                    "dark": "rgba(12, 41, 37, 1)",
                    "contrastText": "#fff"
            },
            "error": {"light": "#e57373", "main": "#f44336", "dark": "#d32f2f", "contrastText": "#fff"},
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default aaronTheme;