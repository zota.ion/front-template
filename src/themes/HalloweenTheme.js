const halloweenTheme = {
    "name":"Halloween theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "rgba(250, 250, 250, 1)", "default": "rgba(255, 221, 212, 1)"},
            "primary": {
                    "light": "rgba(150, 46, 64, 1)",
                    "main": "rgba(94, 23, 66, 1)",
                    "dark": "rgba(51, 1, 54, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(150, 46, 64, 1)",
                    "main": "rgba(94, 23, 66, 1)",
                    "dark": "rgba(51, 1, 54, 1)",
                    "contrastText": "#fff"
            },
            "error": {
                    "light": "#e57373",
                    "main": "rgba(255, 94, 53, 1)",
                    "dark": "rgba(201, 70, 61, 1)",
                    "contrastText": "#fff"
            },
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default halloweenTheme;