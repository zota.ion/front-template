const darkTheme = {    
     palette: {
            common: {black: "#000", white: "#fff"},
            background: {paper: "rgba(250, 250, 250, 1)", default: "rgba(191, 191, 191, 1)"},
            primary: {
                    light: "rgba(115, 115, 115, 1)",
                    main: "rgba(64, 64, 64, 1)",
                    dark: "rgba(13, 13, 13, 1)",
                    contrastText: "#fff"
            },
            secondary: {
                    light: "rgba(115, 115, 115, 1)",
                    main: "rgba(64, 64, 64, 1)",
                    dark: "rgba(13, 13, 13, 1)",
                    contrastText: "#fff"
            },
            error: {light: "#e57373", main: "#f44336", dark: "#d32f2f", contrastText: "#fff"},
            text: {
                    primary: "rgba(0, 0, 0, 0.87)",
                    secondary: "rgba(0, 0, 0, 0.54)",
                    disabled: "rgba(0, 0, 0, 0.38)",
                    hint: "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default darkTheme;