const pastelTheme = {
    "name":"Pastel theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "rgba(250, 250, 250, 1)", "default": "rgba(248, 177, 149, 1)"},
            "primary": {
                    "light": "rgba(192, 108, 132, 1)",
                    "main": "rgba(108, 91, 123, 1)",
                    "dark": "rgba(53, 92, 125, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(192, 108, 132, 1)",
                    "main": "rgba(108, 91, 123, 1)",
                    "dark": "rgba(53, 92, 125, 1)",
                    "contrastText": "#fff"
            },
            "error": {
                    "light": "#e57373",
                    "main": "rgba(255, 94, 53, 1)",
                    "dark": "rgba(201, 70, 61, 1)",
                    "contrastText": "#fff"
            },
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default pastelTheme;