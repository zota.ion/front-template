const rainbowTheme = {
    "name":"Rainbow theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "rgba(250, 250, 250, 1)", "default": "rgba(239, 159, 16, 1)"},
            "primary": {
                    "light": "rgba(239, 223, 74, 1)",
                    "main": "rgba(102, 102, 166, 1)",
                    "dark": "rgba(213, 64, 64, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(239, 223, 74, 1)",
                    "main": "rgba(102, 102, 166, 1)",
                    "dark": "rgba(213, 64, 64, 1)",
                    "contrastText": "#fff"
            },
            "error": {
                    "light": "#e57373",
                    "main": "rgba(255, 94, 53, 1)",
                    "dark": "rgba(201, 70, 61, 1)",
                    "contrastText": "#fff"
            },
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
}

export default rainbowTheme;