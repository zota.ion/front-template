const coralTheme = {
    "name":"Coral theme",
    "palette": {
            "common": {"black": "#000", "white": "#fff"},
            "background": {"paper": "rgba(250, 250, 250, 1)", "default": "rgba(175, 220, 225, 1)"},
            "primary": {
                    "light": "rgba(130, 214, 214, 1)",
                    "main": "rgba(43, 163, 200, 1)",
                    "dark": "rgba(45, 88, 161, 1)",
                    "contrastText": "#fff"
            },
            "secondary": {
                    "light": "rgba(130, 214, 214, 1)",
                    "main": "rgba(43, 163, 200, 1)",
                    "dark": "rgba(45, 88, 161, 1)",
                    "contrastText": "#fff"
            },
            "error": {
                    "light": "#e57373",
                    "main": "rgba(255, 94, 53, 1)",
                    "dark": "rgba(201, 70, 61, 1)",
                    "contrastText": "#fff"
            },
            "text": {
                    "primary": "rgba(0, 0, 0, 0.87)",
                    "secondary": "rgba(0, 0, 0, 0.54)",
                    "disabled": "rgba(0, 0, 0, 0.38)",
                    "hint": "rgba(0, 0, 0, 0.38)"
            }
    },
    typography: {
        useNextVariants: true,
      },
};

export default coralTheme;