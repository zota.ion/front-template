import { FormattedMessage, injectIntl } from "react-intl";
import React from "react";

const Message = props => <FormattedMessage {...props} />;
export default injectIntl(Message, {
  withRef: false
});
