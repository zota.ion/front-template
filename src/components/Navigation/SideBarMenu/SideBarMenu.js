import React, { Component } from "react"
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ListItemIcon, ListItemText } from "@material-ui/core";
import MailIcon from "@material-ui/icons/Mail";
import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import { withRouter } from "react-router-dom";

class SideBarMenu extends Component {

   render() {

    return (       
        <List>        

        <ListItem button key="Home" onClick={ () => this.props.history.push(this.props.url)} >
          <ListItemIcon><HomeIcon /></ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
        <ListItem button key="Inbox" onClick={ () => this.props.history.push(this.props.url+"/inbox")} >
          <ListItemIcon><MailIcon /></ListItemIcon>
          <ListItemText primary="Inbox" />
        </ListItem>

        <ListItem button key="Person" onClick={ () => this.props.history.push(this.props.url+"/person")} >
          <ListItemIcon><PersonIcon /></ListItemIcon>
          <ListItemText primary="Person" />
        </ListItem>         
         
        </List>          
    );
  }
}

export default withRouter(SideBarMenu); 