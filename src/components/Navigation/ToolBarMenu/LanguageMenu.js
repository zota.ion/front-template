import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import React, { Component } from "react";
import * as actions from "../../../store/actions"
import {connect} from "react-redux"

class LanguageMenu extends Component {

  onClickHandler = (props) => {      
      this.props.setLang(props.lang);  
      props.onClose();
  }

  render() {
    return (
      <Menu
        anchorEl={this.props.anchorLanguageEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={this.props.isMenuLanguageOpen}
        onClose={this.props.handleMenuLanguageClose}
      >
        <MenuItem onClick={() => this.onClickHandler({onClose:this.props.handleMenuLanguageClose,lang:"en"})}>English</MenuItem>
        <MenuItem onClick={() => this.onClickHandler({onClose:this.props.handleMenuLanguageClose, lang:"ro"})}>Romanian</MenuItem>
      </Menu>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return {
    setLang: (lang) => dispatch(actions.setLanguage(lang)),
  }
}

export default connect(null, mapDispatchToProps)(LanguageMenu);
