import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { withRouter } from "react-router-dom";

const profileClickHandler = (props) => {
    props.handleMenuClose()
    props.history.push(props.url + "/profile")
}

const accountClickHandler = (props) => {
    props.handleMenuClose()
    props.history.push(props.url + "/account")
}

const logoutClickHandler = (props) => {
  props.handleMenuClose()
  props.history.push("/logout")
}

const profileMenu = (props) => {
return( <Menu
    anchorEl={props.anchorEl}
    anchorOrigin={{ vertical: "top", horizontal: "right" }}
    transformOrigin={{ vertical: "top", horizontal: "right" }}
    open={props.isMenuOpen}
    onClose={props.handleMenuClose}
  >
    <MenuItem onClick={() => profileClickHandler(props)}>Profile</MenuItem>
    <MenuItem onClick={() => accountClickHandler(props)}>My account</MenuItem>
    <MenuItem onClick={() => logoutClickHandler(props)}>Logout</MenuItem>
  </Menu>);

}

export default withRouter(profileMenu);