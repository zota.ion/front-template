import React from "react";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MailIcon from "@material-ui/icons/Mail";
import { withRouter } from "react-router-dom";
import LanguageIcon from "@material-ui/icons/Language";

const messagesClickHandler = (props) => {
    props.handleMobileMenuClose();
    props.history.push(props.url + "/messages")
    }
    
    const notificationsClickHandler = (props) => {
        props.handleMobileMenuClose();
        props.history.push(props.url + "/notifications")
       }

const mobileMenu = (props) => {
    return(
        <Menu
        anchorEl={props.mobileMoreAnchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={props.isMobileMenuOpen}
        onClose={props.handleMenuClose}
      >
        <MenuItem onClick={() => messagesClickHandler(props)}>
        <IconButton color="inherit">
           <Badge badgeContent={4} color="secondary">
               <MailIcon />
            </Badge>
         </IconButton>
          <p>Messages</p>
        </MenuItem>
        <MenuItem onClick={() => notificationsClickHandler(props)}>
        <IconButton color="inherit">
            <Badge badgeContent={3} color="secondary">
               <NotificationsIcon />
          </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={props.handleLanguageMenuOpen}>
          <IconButton color="inherit">
            <LanguageIcon />
          </IconButton>
          <p>Language</p>
        </MenuItem>
        <MenuItem onClick={props.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    );
}

export default withRouter(mobileMenu);