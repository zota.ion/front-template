import React from "react"
import Badge from "@material-ui/core/Badge";
import MailIcon from "@material-ui/icons/Mail";
import IconButton from "@material-ui/core/IconButton";
import LanguageIcon from "@material-ui/icons/Language";
import AccountCircle from "@material-ui/icons/AccountCircle";
import NotificationsIcon from "@material-ui/icons/Notifications";
import { withRouter } from "react-router-dom";

const messagesClickHandler = (props) => {
   props.history.push(props.url + "/messages")
}

const notificationsClickHandler = (props) => {
   props.history.push(props.url + "/notifications")
   }

 const descktopMenu = (props) => {
    
  return (     
     <div className={props.classes.sectionDesktop}> 
        <IconButton
           onClick={() => messagesClickHandler(props)}           
           color="inherit">
           <Badge badgeContent={4} color="secondary">
               <MailIcon />
            </Badge>
         </IconButton>
         <IconButton onClick={() => notificationsClickHandler(props)} color="inherit">
            <Badge badgeContent={3} color="secondary">
               <NotificationsIcon />
          </Badge>
          </IconButton>
          <IconButton 
            aria-owns={props.isMenuLanguageOpen ? "material-appbar" : undefined}
            aria-haspopup="true"
            onClick={props.handleLanguageMenuOpen}
            color="inherit">      
               <LanguageIcon />
         </IconButton>
          <IconButton 
            aria-owns={props.isMenuOpen ? "material-appbar" : undefined}
            aria-haspopup="true"
            onClick={props.handleProfileMenuOpen}
            color="inherit">      
               <AccountCircle />
         </IconButton>          
    </div>
    );
}
export default withRouter(descktopMenu);