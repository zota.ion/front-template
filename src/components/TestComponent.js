import React,{Component} from "react";
import MessageProvider from "./util/MessageProvider"
import Button from "@material-ui/core/Button";

class TestComponent extends Component{

render(){  

    return( <div >
    <header >
      <p><MessageProvider id="app.title"/></p>
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      <Button color="primary">Primary</Button>
      <Button color="secondary">Secondary</Button>

      <div >
              <a href="/?locale=en">English</a>
              <br/>
              <a href="/?locale=ro">Ro</a>
            </div>

    </header>
  </div>)
}
}

export default (TestComponent);