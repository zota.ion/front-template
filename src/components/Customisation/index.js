import React from "react"
import ThemeSwitcher from "./ThemeSwitcher";

const customisation = () => {
    return(
        <ThemeSwitcher/>
    )
}

export default customisation;