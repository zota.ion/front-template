import React, { Component } from "react"
import Switch from "@material-ui/core/Switch";
import {connect} from "react-redux";
import * as actions from "../../store/actions" 

class ThemeSwitcher extends Component {
    
  themeSwitch = (props) =>{
      return(
        <div>          
          <Switch
          checked={props.themeName === this.props.theme}
          onChange={() => this.props.setTheme(props.themeName)}
          value={props.themeName}
          />
          {props.themeName}
          <br/>
        </div>      
      )
  }  

  render() {
    return (
      <div>
         {this.themeSwitch({themeName:"aaronTheme"})} 
         {this.themeSwitch({themeName:"coralTheme"})} 
         {this.themeSwitch({themeName:"darkTheme"})} 
         {this.themeSwitch({themeName:"greenTheme"})} 
         {this.themeSwitch({themeName:"halloweenTheme"})} 
         {this.themeSwitch({themeName:"oceanTheme"})} 
         {this.themeSwitch({themeName:"pastelTheme"})} 
         {this.themeSwitch({themeName:"rainbowTheme"})}          
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
      theme: state.theme.myTheme,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setTheme: (theme) => dispatch(actions.setTheme(theme)),    
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ThemeSwitcher);
