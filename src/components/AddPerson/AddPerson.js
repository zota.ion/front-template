import "./AddPerson.css";
import React from "react";
import { Button, Grid } from "@material-ui/core";

const addPerson = (props) => (
    <Grid   container
    direction="row"
    justify="center"
    alignItems="center">
     <Button color="primary" onClick={props.personAdded}>
         Add Person
     </Button>
    </Grid>
);

export default addPerson;
