import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";

const cache = new InMemoryCache();
const link = new HttpLink({ uri: "http://openstock.eu:8080/graphql" ,
     credentials: "same-origin"})

const authLink = ({token}) => {
  return setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : "",
    }
  }
})};     

const client = (props) => {return new ApolloClient({
     dataIdFromObject: o => o.id,
     link: authLink(props).concat(link),      
     cache: cache
});
}

export default client;
   