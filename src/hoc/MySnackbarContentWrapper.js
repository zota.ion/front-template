import MySnackbarContent, {styles} from "../helpers/MySnachbarContent";
import { withStyles } from "@material-ui/core/styles";

 const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

 export default MySnackbarContentWrapper;