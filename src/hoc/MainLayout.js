import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import SideBarMenu from "../components/Navigation/SideBarMenu/SideBarMenu"
import { Divider } from "@material-ui/core";
import MoreIcon from "@material-ui/icons/MoreVert";
import DescktopMenu from "../components/Navigation/ToolBarMenu/DescktopMenu";
import MobileMenu from "../components/Navigation/ToolBarMenu/MobileMenu";
import ProfileMenu from "../components/Navigation/ToolBarMenu/ProfileMenu";
import LanguageMenu from "../components/Navigation/ToolBarMenu/LanguageMenu";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  grow: {
    flexGrow: 1,
  },
});

class MainLayout extends React.Component {
  state = {
    open: false,
    anchorEl: null,
    mobileMoreAnchorEl: null,
    anchorLanguageEl: null,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  handleLanguageMenuOpen = event => {
    this.setState({ anchorLanguageEl: event.currentTarget });
  };

  handleMenuLanguageClose = () => {
    this.setState({ anchorLanguageEl: null });
    this.handleMobileMenuClose();
  };

  render() {
    const { classes, theme } = this.props;

    const { anchorEl, mobileMoreAnchorEl, anchorLanguageEl } = this.state;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const isMenuLanguageOpen = Boolean(anchorLanguageEl);
    
    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open,
          })}
        >
          <Toolbar disableGutters={!this.state.open}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, {
                [classes.hide]: this.state.open,
              })}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              Mini variant drawer
            </Typography>
            <div className={classes.grow} />            

            <DescktopMenu classes={classes}
                         isMenuOpen = {isMenuOpen}
                         handleProfileMenuOpen={this.handleProfileMenuOpen}
                         url = {this.props.children.props.url}
                         isMenuLanguageOpen = {isMenuLanguageOpen}
                         handleLanguageMenuOpen = {this.handleLanguageMenuOpen}  />
              
            <div className={classes.sectionMobile}>
              <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        <ProfileMenu anchorEl={anchorEl}
                     isMenuOpen = {isMenuOpen}
                     handleMenuClose={this.handleMenuClose}
                     url = {this.props.children.props.url}  
                     />
        <LanguageMenu anchorLanguageEl={anchorLanguageEl}
                      isMenuLanguageOpen = {isMenuLanguageOpen}
                      handleMenuLanguageClose = {this.handleMenuLanguageClose}        
        />             
        <MobileMenu mobileMoreAnchorEl = {mobileMoreAnchorEl}
                    isMobileMenuOpen = {isMobileMenuOpen}
                    handleMenuClose = {this.handleMenuClose}
                    handleProfileMenuOpen = {this.handleProfileMenuOpen}
                    handleMobileMenuClose = {this.handleMobileMenuClose}
                    url = {this.props.children.props.url}
                    isMenuLanguageOpen = {isMenuLanguageOpen}
                    handleMenuLanguageClose = {this.handleMenuLanguageClose}
                    handleLanguageMenuOpen = {this.handleLanguageMenuOpen}
                    />
        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            }),
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider/>
          <SideBarMenu
          url = {this.props.children.props.url}
          />
        
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />          
          {this.props.children}
        </main>
      </div>
    );
  }
}

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(MainLayout);
