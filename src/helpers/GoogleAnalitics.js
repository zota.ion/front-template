import ReactGA from "react-ga";

 const  initializeReactGA = () => {
    ReactGA.initialize("UA-136100798-1");
    ReactGA.pageview("/signin");
}

export default initializeReactGA