import { BrowserRouter, Redirect, Route} from "react-router-dom"
import asyncComponent from "./helpers/AsyncLoader";
import Main from "./containers/Main/Main";
import React from "react";

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  
  <Route
    {...rest}
    render={props =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/signin",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const PublicRoutes = ({isLoggedIn}) => {
    return (
      <BrowserRouter>
        <div>  
        <Route
            exact
            path={"/"}
            component={asyncComponent(() => import("./containers/Auth/Auth"))}
          />

          <Route
            exact
            path={"/signin"}
            component={asyncComponent(() => import("./containers/Auth/Auth"))}
          />

          <Route
            exact
            path={"/logout"}            
            component={asyncComponent(() => import("./containers/Auth/Logout"))}
          />
          
          <RestrictedRoute
            path="/main"
            component={Main}
            isLoggedIn={isLoggedIn}
          />
        </div>
      </BrowserRouter>
  )
}

export default PublicRoutes;
/*

<Route
          exact
          path={"/404"}
          component={asyncComponent(() => import("./containers/Page/404"))}
        />
        <Route
          exact
          path={"/500"}
          component={asyncComponent(() => import("./containers/Page/500"))}
        />
        <Route
          exact
          path={"/signin"}
          component={asyncComponent(() => import("./containers/Page/signin"))}
        />
        <Route
          exact
          path={"/signup"}
          component={asyncComponent(() => import("./containers/Page/signup"))}
        />
        <Route
          exact
          path={"/forgotpassword"}
          component={asyncComponent(() =>
            import("./containers/Page/forgotPassword")
          )}
        />
        <Route
          exact
          path={"/resetpassword"}
          component={asyncComponent(() =>
            import("./containers/Page/resetPassword")
          )}
        />

        <Route
          path="/auth0loginCallback"
          render={props => {
            Auth0.handleAuthentication(props);
          }}
        />

*/